
package progsconsole;

import entites.Client;
import entites.Contrat;
import entites.Technicien;
import javax.persistence.EntityManager;

public class Essais {

    public static void main(String[] args) {
       
        
        // Recherche d'un contrat en utilisant le gestionnaire de persistance
        
        EntityManager em= persistance.Persistance.getEm();
        
        Contrat ct=em.find(Contrat.class, 1001L);
        
        System.out.println("////////////////////////");
        System.out.println();
     
        System.out.println(ct.getLeClient().getNom());
        System.out.println(ct.getMontantContrat());
        
        
        
        // Recherche d'un contrat en utilisant methode statique de la classe Contrat
        
        System.out.println("////////////////////////");
        System.out.println();
        
        Contrat ct2=Contrat.getLeContratNumero(1002L);
        
        System.out.println(ct2.getLeClient().getNom());
        System.out.println(ct2.getMontantContrat());
        
       
        // Recherche d'un tecnicien par son numéro et affichage de son nom, de sa date de naissance et de son ancienneté
        
        
        System.out.println("////////////////////////");
        System.out.println();
        
        Technicien tech= Technicien.getLeTechnicienNumero(801L);
        
        System.out.println(tech.getNom());
        
        System.out.println("né le: "+ utilitaires.UtilDate.format(tech.getDateEmbauche()));
        
        System.out.println( "Ancienneté:"
                
                            +utilitaires.UtilDate.nombreAnneesEcouleesDepuis(tech.getDateEmbauche())+ " ans");
        
        
        
        // Recherche d'un client et de ses contrats
        
        
        System.out.println("////////////////////////");
        System.out.println();
        
        Client cl=em.find(Client.class, 102L);
        
        System.out.println(cl.getNom());
        System.out.println();
        
        for (Contrat cont : cl.getLesContrats()){
             
             System.out.println("  "+cont.getNumero() +  " "+ cont.getMontantContrat()+ " €");
        }
        
    }
}
