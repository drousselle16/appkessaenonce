/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package progsconsole;

import entites.Technicien;

/**
 *
 * @author Administrateur
 */
public class EssaiMethsTechnicien {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Technicien tech = Technicien.getLeTechnicienNumero(801L);
        System.out.println("\nTest de la méthode coût horaire\n");
        System.out.println("Technicien N° : "+tech.getNumero());
        System.out.println("Nom : "+tech.getNom());
        System.out.println("Grade : "+tech.getLeGrade().getLibelle());
        System.out.println("Taux horaire afférent au grade : "+ tech.getLeGrade().getTauxHoraire()+" €");
        System.out.println("Embauché le "+tech.getDateEmbauche()); 
        System.out.println("Anciénneté : "+ tech.getAnciennete());
 
        System.out.println("\nCout Horaire : "+tech.coutHoraireTechnicien()+" €\n");
    }

}
